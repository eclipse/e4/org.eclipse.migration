Correct R3_6_maintenance for org.eclipse.core.commands
Correct R3_6_maintenance for org.eclipse.core.databinding
Correct R3_6_maintenance for org.eclipse.core.databinding.beans
Correct R3_6_maintenance for org.eclipse.core.databinding.observable
Correct R3_6_maintenance for org.eclipse.core.databinding.property
Correct R3_6_maintenance for org.eclipse.jface
Correct R3_6_maintenance for org.eclipse.jface.databinding
Correct R3_6_maintenance for org.eclipse.jface.examples.databinding
Correct R3_6_maintenance for org.eclipse.jface.snippets
Correct R3_6_maintenance for org.eclipse.jface.tests.databinding
Correct R3_6_maintenance for org.eclipse.jface.tests.databinding.conformance
Correct R3_6_maintenance for org.eclipse.ui
Correct R3_6_maintenance for org.eclipse.ui.carbon
Correct R3_6_maintenance for org.eclipse.ui.cocoa
Correct R3_6_maintenance for org.eclipse.ui.examples.contributions
Correct R3_6_maintenance for org.eclipse.ui.examples.fieldassist
Correct R3_6_maintenance for org.eclipse.ui.examples.multipageeditor
Correct R3_6_maintenance for org.eclipse.ui.examples.navigator
Correct R3_6_maintenance for org.eclipse.ui.examples.propertysheet
Correct R3_6_maintenance for org.eclipse.ui.examples.presentation
Correct R3_6_maintenance for org.eclipse.ui.examples.rcp.browser
Correct R3_6_maintenance for org.eclipse.ui.examples.readmetool
Correct R3_6_maintenance for org.eclipse.ui.examples.undo
Correct R3_6_maintenance for org.eclipse.ui.examples.views.properties.tabbed/org.eclipse.ui.examples.views.properties.tabbed.article
Correct R3_6_maintenance for org.eclipse.ui.examples.views.properties.tabbed/org.eclipse.ui.examples.views.properties.tabbed.hockeyleague
Correct R3_6_maintenance for org.eclipse.ui.examples.views.properties.tabbed/org.eclipse.ui.examples.views.properties.tabbed.logic
Correct R3_6_maintenance for org.eclipse.ui.ide
Correct R3_6_maintenance for org.eclipse.ui.ide.application
Correct R3_6_maintenance for org.eclipse.ui.navigator
Correct R3_6_maintenance for org.eclipse.ui.navigator.resources
Correct R3_6_maintenance for org.eclipse.ui.presentations.r21
Correct R3_6_maintenance for org.eclipse.ui.tests
Correct R3_6_maintenance for org.eclipse.ui.tests.harness
Correct R3_6_maintenance for org.eclipse.ui.tests.navigator
Correct R3_6_maintenance for org.eclipse.ui.tests.performance
Correct R3_6_maintenance for org.eclipse.ui.tests.rcp
Correct R3_6_maintenance for org.eclipse.ui.tests.views.properties.tabbed
Correct R3_6_maintenance for org.eclipse.ui.views
Correct R3_6_maintenance for org.eclipse.ui.views.properties.tabbed
Correct R3_6_maintenance for org.eclipse.ui.win32
Correct R3_6_maintenance for org.eclipse.ui.workbench
Correct R3_6_maintenance for org.eclipse.ui.workbench.compatibility
Correct R3_6_maintenance for org.eclipse.ui.browser
cvs rtag -D"06/04/2010 00:00" R3_6 org.eclipse.ui.examples.job
cvs rtag -b -rR3_6 R3_6_maintenance org.eclipse.ui.examples.job
Correct R3_6_maintenance for org.eclipse.ui.forms
cvs rtag -D"06/04/2010 00:00" R3_6 org.eclipse.ui.forms.examples
cvs rtag -b -rR3_6 R3_6_maintenance org.eclipse.ui.forms.examples
cvs rtag -D"06/04/2010 00:00" R3_6 org.eclipse.ui.tests.browser
cvs rtag -b -rR3_6 R3_6_maintenance org.eclipse.ui.tests.browser
cvs rtag -b -rR3_6 R3_6_maintenance org.eclipse.ui.tests.forms
cvs rtag -D"06/04/2010 00:00" R3_6 org.eclipse.ui.tutorials.rcp.part1
cvs rtag -b -rR3_6 R3_6_maintenance org.eclipse.ui.tutorials.rcp.part1
cvs rtag -D"06/04/2010 00:00" R3_6 org.eclipse.ui.tutorials.rcp.part2
cvs rtag -b -rR3_6 R3_6_maintenance org.eclipse.ui.tutorials.rcp.part2
cvs rtag -D"06/04/2010 00:00" R3_6 org.eclipse.ui.tutorials.rcp.part3
cvs rtag -b -rR3_6 R3_6_maintenance org.eclipse.ui.tutorials.rcp.part3
