
MODULES='org.eclipse.core.commands
org.eclipse.core.databinding
org.eclipse.core.databinding.beans
org.eclipse.core.databinding.observable
org.eclipse.core.databinding.property
org.eclipse.jface
org.eclipse.jface.databinding
org.eclipse.jface.examples.databinding
org.eclipse.jface.snippets
org.eclipse.jface.tests.databinding
org.eclipse.jface.tests.databinding.conformance
org.eclipse.ui
org.eclipse.ui.carbon
org.eclipse.ui.cocoa
org.eclipse.ui.examples.contributions
org.eclipse.ui.examples.fieldassist
org.eclipse.ui.examples.multipageeditor
org.eclipse.ui.examples.navigator
org.eclipse.ui.examples.propertysheet
org.eclipse.ui.examples.presentation
org.eclipse.ui.examples.rcp.browser
org.eclipse.ui.examples.readmetool
org.eclipse.ui.examples.undo
org.eclipse.ui.examples.views.properties.tabbed/org.eclipse.ui.examples.views.properties.tabbed.article
org.eclipse.ui.examples.views.properties.tabbed/org.eclipse.ui.examples.views.properties.tabbed.hockeyleague
org.eclipse.ui.examples.views.properties.tabbed/org.eclipse.ui.examples.views.properties.tabbed.logic
org.eclipse.ui.ide
org.eclipse.ui.ide.application
org.eclipse.ui.navigator
org.eclipse.ui.navigator.resources
org.eclipse.ui.presentations.r21
org.eclipse.ui.tests
org.eclipse.ui.tests.harness
org.eclipse.ui.tests.navigator
org.eclipse.ui.tests.performance
org.eclipse.ui.tests.rcp
org.eclipse.ui.tests.views.properties.tabbed
org.eclipse.ui.views
org.eclipse.ui.views.properties.tabbed
org.eclipse.ui.win32
org.eclipse.ui.workbench
org.eclipse.ui.workbench.compatibility

org.eclipse.ui.browser
org.eclipse.ui.examples.job
org.eclipse.ui.forms
org.eclipse.ui.forms.examples
org.eclipse.ui.tests.browser
org.eclipse.ui.tests.forms
org.eclipse.ui.tutorials.rcp.part1
org.eclipse.ui.tutorials.rcp.part2
org.eclipse.ui.tutorials.rcp.part3

'

# legacy projects I won't move
# org.eclipse.core.components
# org.eclipse.ui.examples.components
# org.eclipse.ui.internal.r21presentation
# org.eclipse.jface.databinding.beans
# org.eclipse.jface.databinding.ui
# org.eclipse.ui.versioncheck


