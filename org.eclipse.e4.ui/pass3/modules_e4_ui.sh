
MODULES='
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.core.javascript
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.pde.ui
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.pde.webui
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.ui.css.jface
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.ui.css.legacy
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.ui.css.nebula
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.ui.gadgets
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.ui.selection
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.ui.web
e4/org.eclipse.e4.ui/bundles/org.eclipse.e4.ui.workbench.fragment
e4/org.eclipse.e4.ui/tests/org.eclipse.e4.ui.selection.tests
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.e4photo.flickr
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.e4photo.flickr.mock
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.e4photo.flickr.service
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.e4photo.flickr.service.rest
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.log
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.minimal
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.modifier
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.dialogs
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.e4editor
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.editor
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.editor.text
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.iconview
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.jdt
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.model
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.navigator
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.outline
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.services
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.simpleide.sharedimages
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.tools.simpleide
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.tools.simpleide3x
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.demo.views
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.ui.examples.css.nebula
e4/org.eclipse.e4.ui/examples/org.eclipse.e4.ui.examples.legacy.workbench
e4/org.eclipse.e4.ui/features/org.eclipse.e4.master
e4/org.eclipse.e4.ui/features/org.eclipse.e4.sdk.runtime.feature
e4/org.eclipse.e4.ui/features/org.eclipse.e4.sdk.source.feature
e4/org.eclipse.e4.ui/features/org.eclipse.e4.ui.compatibility.feature
e4/org.eclipse.e4.ui/features/org.eclipse.e4.ui.css.feature
e4/org.eclipse.e4.ui/features/org.eclipse.e4.ui.examples.feature
e4/org.eclipse.e4.ui/features/org.eclipse.e4.ui.feature
e4/org.eclipse.e4.ui/features/org.eclipse.e4.ui.tests.feature
e4/org.eclipse.e4.ui/features/org.eclipse.e4.ui.web.feature


'

# a 3rd party lib that should go in Orbit
# org.pushingpixels.trident

