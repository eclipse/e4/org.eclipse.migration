#!/bin/bash
# *** Don't run this script :-) ***

ROOT=/shared/eclipse/e4/git/cvs
MIGR=$ROOT/org.eclipse.migration

##################################################
# first up, the big one: eclipse.platform.ui

# pre-conditioning step

# run the retag script:
  ../../scripts/retag_cvs.sh modules_ui.sh R3_6 "06/04/2010 00:00" R3_6_maintenance >tag_pass1.sh
  grep ^cvs tag_pass1.sh >tag_fix1.sh
  #examine tag_fix1.sh and then run
  /bin/bash -x tag_fix1.sh
  #re-run retag script
  ../../scripts/retag_cvs.sh modules_ui.sh R3_6 "06/04/2010 00:00" R3_6_maintenance >tag_pass2.sh


# Then fix the other tags.  TAG1 is a known tag (like from above :-)
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_6 R3_6_1 >subtag_pass1.sh
  grep ^cvs subtag_pass1.sh >subtag_fix1.sh
  #examine subtag_fix1.sh and then run
  /bin/bash -x subtag_fix1.sh
  #re-run subtag script to verify everybody is tagged
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_6 R3_6_1 >subtag_pass2.sh

#
# you have to repeat the above for every tag/branch you care about
#
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_6_1 R3_6_2 >subtag_pass3.sh
  grep ^cvs subtag_pass3.sh >subtag_fix3.sh
  /bin/bash -x subtag_fix3.sh

  # do R3_5_maintenance
  ../../scripts/retag_cvs.sh modules_ui.sh R3_5 "06/05/2009 00:00" R3_5_maintenance >tag35_pass1.sh
  grep ^cvs tag35_pass1.sh >tag35_fix1.sh
  /bin/bash -x tag35_fix1.sh >log35_1.txt
  ../../scripts/retag_cvs.sh modules_ui.sh R3_5 "06/05/2009 00:00" R3_5_maintenance >tag35_pass2.sh

  # R3_5_1, R3_5_2
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_5 R3_5_1 >sub35_pass1.sh
  grep ^cvs sub35_pass1.sh >sub35_fix1.sh
  /bin/bash -x sub35_fix1.sh
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_5_1 R3_5_2 >sub35_pass2.sh
  grep ^cvs sub35_pass2.sh >sub35_fix2.sh
  /bin/bash -x sub35_fix2.sh

  # do R3_4_maintenance
  ../../scripts/retag_cvs.sh modules_ui.sh R3_4 "06/11/2008 00:00" R3_4_maintenance >tag34_pass1.sh
  grep ^cvs tag34_pass1.sh >tag34_fix1.sh
  /bin/bash -x tag34_fix1.sh
  ../../scripts/retag_cvs.sh modules_ui.sh R3_4 "06/11/2008 00:00" R3_4_maintenance >tag34_pass2.sh

  # do R3_4_1, R3_4_2
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_4 R3_4_1 >sub34_pass1.sh
  grep ^cvs sub34_pass1.sh >sub34_fix1.sh
  # this says some random parts of org.eclipse.core.databinding.observable already tagged
  # W org.eclipse.core.databinding.observable/src/org/eclipse/core/databinding/observable/list/ObservableList.java : R3_4_1 already exists on version 1.12.2.1 : NOT MOVING tag to version 1.12
  # W org.eclipse.core.databinding.observable/src/org/eclipse/core/databinding/observable/map/AbstractObservableMap.java : R3_4_1 already exists on version 1.8.2.1 : NOT MOVING tag to version 1.8
  # W org.eclipse.core.databinding.observable/src/org/eclipse/core/databinding/observable/map/ObservableMap.java : R3_4_1 already exists on version 1.7.4.1 : NOT MOVING tag to version 1.7
  # W org.eclipse.core.databinding.observable/src/org/eclipse/core/databinding/observable/set/ObservableSet.java : R3_4_1 already exists on version 1.6.2.1 : NOT MOVING tag to version 1.6
  # W org.eclipse.core.databinding.observable/src/org/eclipse/core/internal/databinding/observable/UnmodifiableObservableValue.java : R3_4_1 already exists on version 1.2.2.2 : NOT MOVING tag to version 1.2
  /bin/bash -x sub34_fix1.sh

  ../../scripts/subtag_cvs.sh modules_ui.sh R3_4_1 R3_4_2 >sub34_pass2.sh
  grep ^cvs sub34_pass2.sh >sub34_fix2.sh
  /bin/bash -x sub34_fix2.sh

  # do R3_3_maintenance
  ../../scripts/retag_cvs.sh modules_ui.sh R3_3 "06/14/2007 00:00" R3_3_maintenance >tag33_pass1.sh
  grep ^cvs tag33_pass1.sh >tag33_fix1.sh
  /bin/bash -x tag33_fix1.sh

  # do R3_3_1 and R3_3_2
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_3 R3_3_1 >sub33_pass1.sh
  grep ^cvs sub33_pass1.sh >sub33_fix1.sh
  /bin/bash -x sub33_fix1.sh
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_3_1 R3_3_2 >sub33_pass2.sh
  grep ^cvs sub33_pass2.sh >sub33_fix2.sh
  /bin/bash -x sub33_fix2.sh

  # do R3_2_maintenance
  ../../scripts/retag_cvs.sh modules_ui.sh R3_2 "06/06/2006 00:00" R3_2_maintenance >tag32_pass1.sh
  grep ^cvs tag32_pass1.sh >tag32_fix1.sh
  /bin/bash -x tag32_fix1.sh

  # R3_2_1, R3_2_2
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_2 R3_2_1 >sub32_pass1.sh
  grep ^cvs sub32_pass1.sh >sub32_fix1.sh
  /bin/bash -x sub32_fix1.sh
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_2_1 R3_2_2 >sub32_pass2.sh
  grep ^cvs sub32_pass2.sh >sub32_fix2.sh
  /bin/bash -x sub32_fix2.sh

  # do R3_1_maintenance
  ../../scripts/retag_cvs.sh modules_ui.sh R3_1 "06/28/2005 00:00" R3_1_maintenance >tag31_pass1.sh
  grep ^cvs tag31_pass1.sh >tag31_fix1.sh
  /bin/bash -x tag31_fix1.sh

  # do R3_1_1, R3_1_2
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_1 R3_1_1 >sub31_pass1.sh
  grep ^cvs sub31_pass1.sh >sub31_fix1.sh
  /bin/bash -x sub31_fix1.sh
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_1_1 R3_1_2 >sub31_pass2.sh
  grep ^cvs sub31_pass2.sh >sub31_fix2.sh
  /bin/bash -x sub31_fix2.sh

  # try R3_0_maintenance :-)
  ../../scripts/retag_cvs.sh modules_ui.sh R3_0 "06/25/2004 00:00" R3_0_maintenance >tag30_pass1.sh
  grep ^cvs tag30_pass1.sh >tag30_fix1.sh
  /bin/bash -x tag30_fix1.sh

  # do R3_0_1, R3_0_2
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_0 R3_0_1 >sub30_pass1.sh
  grep ^cvs sub30_pass1.sh >sub30_fix1.sh
  /bin/bash -x sub30_fix1.sh
  ../../scripts/subtag_cvs.sh modules_ui.sh R3_0_1 R3_0_2 >sub30_pass2.sh
  grep ^cvs sub30_pass2.sh >sub30_fix2.sh
  /bin/bash -x sub30_fix2.sh

  # ooops, do R3_7_maintenance
  ../../scripts/retag_cvs.sh modules_ui.sh R3_7 "06/24/2011 00:00" R3_7_maintenance >tag37_pass1.sh
  grep ^cvs tag37_pass1.sh >tag37_fix1.sh
  /bin/bash -x tag37_fix1.sh




# make the fake cvs repo in the right shape
CVS_LOC=$ROOT/cvsroot.eclipse.platform.ui
mkdir $CVS_LOC
cd $CVS_LOC
cvs -d :local:$(pwd) init
mkdir -p eclipse.platform.ui
cd eclipse.platform.ui
mkdir bundles  examples  features  tests


MIGR_PASS=$MIGR/eclipse.platform.ui/pass3

cd /cvsroot/eclipse
. $MIGR_PASS/modules_ui.sh
for M in $MODULES; do
  echo $M
  cp -r $M $CVS_LOC/eclipse.platform.ui/bundles
done

cd $CVS_LOC/eclipse.platform.ui/bundles
mv *tests* ../tests
mv *example* *snippet* ../examples

# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

#get the tool, once
cd $ROOT
svn co --username=guest --password="" http://cvs2svn.tigris.org/svn/cvs2svn/trunk cvs2svn-trunk

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt $MIGR_PASS


# create a git repo to be used for verification
git init verify.eclipse.platform.ui
cd verify.eclipse.platform.ui
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

git tag -d 'TC/EJPsnapshot134a'

# verify the repo - started on work linux box
cd $ROOT
mkdir compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $CVS_LOC/eclipse.platform.ui \
$ROOT/verify.eclipse.platform.ui \
--tmp=$ROOT/compare-tmp --diff >$MIGR_PASS/compare-log.txt 2>&1 &

# create a git repo to continue the conversion
git init target.eclipse.platform.ui
cd target.eclipse.platform.ui




cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250


# we also have to delete at least 2 tags:
git tag 'R3_6_api_cleanup_build_20091125' 'R3_6_api_cleanup_<build_20091125>'
git tag -d 'R3_6_api_cleanup_<build_20091125>'
git tag 'R3_6_p2_api_cleanup_20091216' 'R3_6_p2_api_cleanup_<20091216>'
git tag -d 'R3_6_p2_api_cleanup_<20091216>'
git tag -d 'TC/EJPsnapshot134a'

# Fixing the Delete only tags:
cd $ROOT
/bin/bash $MIGR/scripts/fix_tags.sh >$MIGR_PASS/tag_report.sh
grep ^git $MIGR_PASS/tag_report.sh >$MIGR_PASS/mv_tags.sh
# examine them
/bin/bash $MIGR_PASS/mv_tags.sh

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.

grep ^Cherry $MIGR_PASS/tag_report.sh >$MIGR_PASS/tag_cherrypicks.txt
grep "R[34]_" $MIGR_PASS/tag_cherrypicks.txt
#Cherrypick tag: R3_0 commit: ead7a690ae2a6b78507ee0705e2cdd193c4c9dfd
#Cherrypick tag: R3_0_2 commit: 33b9f9f25fd8e45cd2206a504725bcfb03e9199f
#Cherrypick tag: R3_1 commit: 83d5b3b8596bf06ceea3cf0054dd6c66e049f054
#Cherrypick tag: R3_3_1_1 commit: a74e9bad283b3d01ec5683fa719710e9ca0e645b
#Cherrypick tag: R3_4_1 commit: 96e8167b3756219d62dcaf5318c6a42f1b93dfb9
#Cherrypick tag: R3_5 commit: 4186af7444f9685e7f4bf27cfe1878866ce9a2e5
#Cherrypick tag: Root_R3_4_maintenance commit: e4ec86e43c2cec1e0d6e94660bed1704d5eaf41d

git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt





#
# after the other repos are converted, the next step is the merge with eclipse.platform.ui.e4
#


##################################################
# next, org.eclipse.e4.tools since it should be isolated
MODULE_NAME=org.eclipse.e4.tools

cd $ROOT
CVS_LOC=$ROOT/cvsroot.$MODULE_NAME
mkdir -p $CVS_LOC/e4
cd $CVS_LOC
cvs -d :local:$(pwd) init

MIGR_PASS=$MIGR/org.eclipse.e4.tools/pass3
cp -r /cvsroot/eclipse/e4/$MODULE_NAME $CVS_LOC/e4

# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cd $ROOT
vi $MIGR_PASS/cvs2git.options
rm -rf cvs2svn-tmp symbol-info.txt cvs2git_log.txt
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt cvs2git_log.txt $MIGR_PASS



# create a git repo to be used for verification
git init verify.$MODULE_NAME
cd verify.$MODULE_NAME
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

# verify the repo - this can take long, so for your first test skip this step.
cd $ROOT
rm -rf compare-tmp
mkdir compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $CVS_LOC/e4/$MODULE_NAME \
$ROOT/verify.$MODULE_NAME \
--tmp=$ROOT/compare-tmp --diff >$MIGR_PASS/compare-log.txt 2>&1 &

# create a git repo to continue the conversion
cd $ROOT
git init target.$MODULE_NAME
cd target.$MODULE_NAME

cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250



# Fixing the Delete only tags:
cd $ROOT/target.$MODULE_NAME
/bin/bash $MIGR/scripts/fix_tags.sh >$MIGR_PASS/tag_report.sh
grep ^git $MIGR_PASS/tag_report.sh >$MIGR_PASS/mv_tags.sh
# examine them
/bin/bash $MIGR_PASS/mv_tags.sh

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.

grep ^Cherry $MIGR_PASS/tag_report.sh >$MIGR_PASS/tag_cherrypicks.txt
grep "R[34]_" $MIGR_PASS/tag_cherrypicks.txt
# No R?_ Cherry pick tags

git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt

# last step before you deploy:

cd $ROOT
cat - >gitignore <<EOF
bin/
*~
*.rej
*.bak
*.patch
javacore.*
heapdump.*
core.*
Snap.*
target/

EOF

cd target.$MODULE_NAME

# from http://stackoverflow.com/questions/3895453/how-do-i-make-a-git-commit-in-the-past

new_file=$(git hash-object -w ../gitignore)
git filter-branch \
--index-filter \
'git update-index --add --cacheinfo 100644 '"$new_file"' .gitignore' \
--tag-name-filter cat \
-- --all

git reset --hard

#create deploy
cd $ROOT
git clone --bare  target.$MODULE_NAME deploy.$MODULE_NAME
cd deploy.$MODULE_NAME
git remote rm origin
git config -l
git config --add core.sharedrepository 1

#final pack and prune step
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

cd $ROOT
scp -r deploy.$MODULE_NAME pwebster@git.eclipse.org:/gitroot/e4/${MODULE_NAME}.git
#
# DONE, org.eclipse.e4.tools
#



##################################################
# next, eclipse.platform.ui.e4 - graduated Eclipse4 UI bundles + compat layer
MODULE_NAME=eclipse.platform.ui.e4

cd $ROOT
#CVS_LOC=$ROOT/cvsroot.$MODULE_NAME
#mkdir -p $CVS_LOC/e4
#cd $CVS_LOC
#cvs -d :local:$(pwd) init
# using existing version for this pass, put compat workbench in it

MIGR_PASS=$MIGR/$MODULE_NAME/pass4
#$MIGR/scripts/copy_e4.sh $MIGR_PASS/modules_platform_ui.sh $CVS_LOC

# fix up the compat tests that moved into platform ui
# already done


# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cd $ROOT
vi $MIGR_PASS/cvs2git.options
rm -rf cvs2svn-tmp symbol-info.txt cvs2git_log.txt
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt cvs2git_log.txt $MIGR_PASS



# create a git repo to be used for verification
git init verify.$MODULE_NAME
cd verify.$MODULE_NAME
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

# verify the repo - this can take long, so for your first test skip this step.
cd $ROOT
rm -rf compare-tmp
mkdir compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $CVS_LOC/e4/$MODULE_NAME \
$ROOT/verify.$MODULE_NAME \
--tmp=$ROOT/compare-tmp --diff >$MIGR_PASS/compare-log.txt 2>&1 &

# create a git repo to continue the conversion
cd $ROOT
git init target.$MODULE_NAME
cd target.$MODULE_NAME

cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250



# Fixing the Delete only tags:
cd $ROOT/target.$MODULE_NAME
/bin/bash $MIGR/scripts/fix_tags.sh >$MIGR_PASS/tag_report.sh
grep ^git $MIGR_PASS/tag_report.sh >$MIGR_PASS/mv_tags.sh
# examine them
/bin/bash $MIGR_PASS/mv_tags.sh

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.

grep ^Cherry $MIGR_PASS/tag_report.sh >$MIGR_PASS/tag_cherrypicks.txt
grep "R[34]_" $MIGR_PASS/tag_cherrypicks.txt
# No changes here

git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt

# the next step for this one is to be merged into eclipse.platform.ui



##################################################
# next, eclipse.platform.runtime.e4 - graduated Eclipse4 core bundles
MODULE_NAME=eclipse.platform.runtime.e4

cd $ROOT
CVS_LOC=$ROOT/cvsroot.$MODULE_NAME
mkdir -p $CVS_LOC/e4
cd $CVS_LOC
cvs -d :local:$(pwd) init

MIGR_PASS=$MIGR/$MODULE_NAME/pass3
$MIGR/scripts/copy_e4.sh $MIGR_PASS/modules_platform_runtime.sh $CVS_LOC



# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cd $ROOT
vi $MIGR_PASS/cvs2git.options
rm -rf cvs2svn-tmp symbol-info.txt cvs2git_log.txt
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt cvs2git_log.txt $MIGR_PASS



# create a git repo to be used for verification
git init verify.$MODULE_NAME
cd verify.$MODULE_NAME
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

# verify the repo - on a linux box, must contain cvsroot and verify.repo
VERIFY_DIR=$ROOT/verify-$MODULE_NAME
cd $VERIFY_DIR
# comment out  UseNewInfoFmtStrings
vi cvsroot.$MODULE_NAME/CVSROOT/config

rm -rf $VERIFY_DIR/compare-tmp
mkdir  $VERIFY_DIR/compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $VERIFY_DIR/cvsroot.$MODULE_NAME/e4/org.eclipse.e4.ui \
$VERIFY_DIR/verify.$MODULE_NAME \
--tmp=$VERIFY_DIR/compare-tmp --diff >$MIGR_PASS/verify-compare-log.txt 2>&1 &

# create a git repo to continue the conversion
cd $ROOT
git init target.$MODULE_NAME
cd target.$MODULE_NAME

cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250



# Fixing the Delete only tags:
cd $ROOT/target.$MODULE_NAME
/bin/bash $MIGR/scripts/fix_tags.sh >$MIGR_PASS/tag_report.sh
grep ^git $MIGR_PASS/tag_report.sh >$MIGR_PASS/mv_tags.sh
# examine them
/bin/bash $MIGR_PASS/mv_tags.sh

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.

grep ^Cherry $MIGR_PASS/tag_report.sh >$MIGR_PASS/tag_cherrypicks.txt
grep "R[34]_" $MIGR_PASS/tag_cherrypicks.txt
# No changes here

git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt

# the next step for this one is to be merged into eclipse.platform.runtime ... still to be converted





##################################################
# next, org.eclipse.e4.ui - leftover e4 bundles and simpleide
MODULE_NAME=org.eclipse.e4.ui

cd $ROOT
CVS_LOC=$ROOT/cvsroot.$MODULE_NAME
mkdir -p $CVS_LOC/e4
cd $CVS_LOC
cvs -d :local:$(pwd) init

MIGR_PASS=$MIGR/$MODULE_NAME/pass3
$MIGR/scripts/copy_e4.sh $MIGR_PASS/modules_e4_ui.sh $CVS_LOC



# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cd $ROOT
vi $MIGR_PASS/cvs2git.options
rm -rf cvs2svn-tmp symbol-info.txt cvs2git_log.txt
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt cvs2git_log.txt $MIGR_PASS



# create a git repo to be used for verification
git init verify.$MODULE_NAME
cd verify.$MODULE_NAME
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

# verify the repo - this can take long, so for your first test skip this step.
cd $ROOT
rm -rf compare-tmp
mkdir compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $CVS_LOC/e4/$MODULE_NAME \
$ROOT/verify.$MODULE_NAME \
--tmp=$ROOT/compare-tmp --diff >$MIGR_PASS/compare-log.txt 2>&1 &

# create a git repo to continue the conversion
cd $ROOT
git init target.$MODULE_NAME
cd target.$MODULE_NAME

cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250



# Fixing the Delete only tags:
cd $ROOT/target.$MODULE_NAME
/bin/bash $MIGR/scripts/fix_tags.sh >$MIGR_PASS/tag_report.sh
grep ^git $MIGR_PASS/tag_report.sh >$MIGR_PASS/mv_tags.sh
# examine them
/bin/bash $MIGR_PASS/mv_tags.sh

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.

grep ^Cherry $MIGR_PASS/tag_report.sh >$MIGR_PASS/tag_cherrypicks.txt
grep "R[34]_" $MIGR_PASS/tag_cherrypicks.txt
# No changes here

git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt

cd $ROOT
cat - >gitignore <<EOF
bin/
*~
*.rej
*.bak
*.patch
javacore.*
heapdump.*
core.*
Snap.*
target/

EOF

cd target.$MODULE_NAME

# from http://stackoverflow.com/questions/3895453/how-do-i-make-a-git-commit-in-the-past
rm -f t1.txt
git checkout master

new_file=$(git hash-object -w ../gitignore)
git filter-branch \
--index-filter \
'git update-index --add --cacheinfo 100644 '"$new_file"' .gitignore' \
--tag-name-filter cat \
-- --all

git reset --hard

#create deploy
cd $ROOT
git clone --bare  target.$MODULE_NAME deploy.$MODULE_NAME
cd deploy.$MODULE_NAME
git remote rm origin
git config -l
git config --add core.sharedrepository 1

#final pack and prune step
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

cd $ROOT
scp -r deploy.$MODULE_NAME pwebster@git.eclipse.org:/gitroot/e4/${MODULE_NAME}.git
#
# DONE, org.eclipse.e4.ui
#


##################################################
# next, eclipse.platform.ui.compat - The ui.workbench fork - just rolled it into e4.ui


##################################################
# Let's do eclipse.platform.runtime so core has a place to go

#from somewhere with a useful CVSROOT
../../scripts/retag_cvs.sh modules_runtime.sh R3_6 "06/04/2010 00:00" R3_6_maintenance >tag_pass1.sh
grep ^cvs tag_pass1.sh >tag_fix1.sh
#examine tag_fix1.sh and then run
/bin/bash -x tag_fix1.sh

# Then fix the other tags.  TAG1 is a known tag (like from above :-)
../../scripts/subtag_cvs.sh modules_runtime.sh R3_6 R3_6_1 >subtag_pass1.sh
grep ^cvs subtag_pass1.sh >subtag_fix1.sh
#examine subtag_fix1.sh and then run
/bin/bash -x subtag_fix1.sh

../../scripts/subtag_cvs.sh modules_runtime.sh R3_6_1 R3_6_2 >subtag_pass2.sh
grep ^cvs subtag_pass2.sh >subtag_fix2.sh
#examine subtag_fix2.sh and then run
/bin/bash -x subtag_fix2.sh


# R3_5
../../scripts/retag_cvs.sh modules_runtime.sh R3_5 "06/05/2009 00:00" R3_5_maintenance  >tag35_pass1.sh
grep ^cvs tag35_pass1.sh >tag35_fix1.sh
#examine tag35_fix1.sh and then run
/bin/bash -x tag35_fix1.sh

# Then fix the other tags.  TAG1 is a known tag (like from above :-)
../../scripts/subtag_cvs.sh modules_runtime.sh R3_5 R3_5_1 >subtag35_pass1.sh
grep ^cvs subtag35_pass1.sh >subtag35_fix1.sh
#examine subtag35_fix1.sh and then run
/bin/bash -x subtag35_fix1.sh

../../scripts/subtag_cvs.sh modules_runtime.sh R3_5_1 R3_5_2 >subtag35_pass2.sh
grep ^cvs subtag35_pass2.sh >subtag35_fix2.sh
#examine subtag35_fix2.sh and then run
/bin/bash -x subtag35_fix2.sh

# R3_4
../../scripts/retag_cvs.sh modules_runtime.sh R3_4 "06/11/2008 00:00" R3_4_maintenance  >tag34_pass1.sh
grep ^cvs tag34_pass1.sh >tag34_fix1.sh
#examine tag34_fix1.sh and then run
/bin/bash -x tag34_fix1.sh

# Then fix the other tags.  TAG1 is a known tag (like from above :-)
../../scripts/subtag_cvs.sh modules_runtime.sh R3_4 R3_4_1 >subtag34_pass1.sh
grep ^cvs subtag34_pass1.sh >subtag34_fix1.sh
#examine subtag34_fix1.sh and then run
/bin/bash -x subtag34_fix1.sh

../../scripts/subtag_cvs.sh modules_runtime.sh R3_4_1 R3_4_2 >subtag34_pass2.sh
grep ^cvs subtag34_pass2.sh >subtag34_fix2.sh
#examine subtag34_fix2.sh and then run
/bin/bash -x subtag34_fix2.sh

# R3_7
../../scripts/retag_cvs.sh modules_runtime.sh R3_7 "06/24/2011 00:00" R3_7_maintenance >tag37_pass1.sh
grep ^cvs tag37_pass1.sh >tag37_fix1.sh
#examine tag37_fix1.sh and then run
/bin/bash -x tag37_fix1.sh

# let's get started
MODULE_NAME=eclipse.platform.runtime

cd $ROOT
CVS_LOC=$ROOT/cvsroot.$MODULE_NAME
mkdir -p $CVS_LOC/eclipse.platform.runtime/bundles
cd $CVS_LOC
cvs -d :local:$(pwd) init

MIGR_PASS=$MIGR/$MODULE_NAME/pass3

cd /cvsroot/eclipse
. $MIGR_PASS/modules_runtime.sh
for M in $MODULES; do
  echo $M
  cp -r $M $CVS_LOC/eclipse.platform.runtime/bundles
done



# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct - updated for eclipse.platform.runtime
# make sure your options file points to the correct tmp cvsroot
cd $ROOT
vi $MIGR_PASS/cvs2git.options
rm -rf cvs2svn-tmp symbol-info.txt cvs2git_log.txt
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt cvs2git_log.txt $MIGR_PASS



# create a git repo to be used for verification
git init verify.$MODULE_NAME
cd verify.$MODULE_NAME
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

# verify the repo - done on linux box
VERIFY_DIR=$ROOT/verify-$MODULE_NAME
cd $VERIFY_DIR
# comment out  UseNewInfoFmtStrings
vi cvsroot.$MODULE_NAME/CVSROOT/config

rm -rf $VERIFY_DIR/compare-tmp
mkdir  $VERIFY_DIR/compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $VERIFY_DIR/cvsroot.$MODULE_NAME/$MODULE_NAME \
$VERIFY_DIR/verify.$MODULE_NAME \
--tmp=$VERIFY_DIR/compare-tmp --diff >$MIGR_PASS/verify-compare-log.txt 2>&1 &

# create a git repo to continue the conversion
cd $ROOT
git init target.$MODULE_NAME
cd target.$MODULE_NAME

cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250



# Fixing the Delete only tags:
cd $ROOT/target.$MODULE_NAME
/bin/bash $MIGR/scripts/fix_tags.sh >$MIGR_PASS/tag_report.sh
grep ^git $MIGR_PASS/tag_report.sh >$MIGR_PASS/mv_tags.sh
# examine them
/bin/bash $MIGR_PASS/mv_tags.sh

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.

grep ^Cherry $MIGR_PASS/tag_report.sh >$MIGR_PASS/tag_cherrypicks.txt
grep "R[34]_" $MIGR_PASS/tag_cherrypicks.txt
#Cherrypick tag: R3_0_1 commit: 97501471bd7c6703f61c0f0ac8a7d9d6cbce26a7
#Cherrypick tag: R3_0_2 commit: 12ec8ee0788365599a1a4ad48cbf0afd7c377b23
#Cherrypick tag: R3_1_1 commit: 202d867ac7ed58a4fea01bdc7835be94c8ac9a34
#Cherrypick tag: R3_1_2 commit: d7dfd6ff907e77ca9f310b3f27e5402fba8214dd
#Cherrypick tag: R3_2_1 commit: cdbda0401288a0255427acd5ef5c0c4c9b91a49c
#Cherrypick tag: R3_2_2 commit: ad5844f5da7b5d688eb799d90eda450a952892e4
#Cherrypick tag: R3_3_1 commit: 7e0126e6acc3aef7229414450c973b4a2e30dc3f
#Cherrypick tag: R3_3_1_1 commit: 7e0126e6acc3aef7229414450c973b4a2e30dc3f
#Cherrypick tag: R3_3_2 commit: 7e0126e6acc3aef7229414450c973b4a2e30dc3f
#Cherrypick tag: R3_5 commit: 0fcdd52072d2283d9ebb39a03630caefb65614dd


git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt

# now we're ready to start stiching repos together :-)

##################################################
# merging eclipse.platform.runtime.e4 into eclipse.platform.runtime

MODULE_NAME=eclipse.platform.runtime
MIGR_PASS=$MIGR/$MODULE_NAME/pass3
MIGR_E4_PASS=$MIGR/eclipse.platform.runtime.e4/pass3

cd $ROOT
comm -12 $MIGR_PASS/tags.txt $MIGR_E4_PASS/tags.txt >$MIGR_PASS/common_tags.txt
comm -12 $MIGR_PASS/branches.txt $MIGR_E4_PASS/branches.txt >$MIGR_PASS/common_branches.txt

# no common branches or tags ... good

cd target.$MODULE_NAME
git config user.email pwebster
git config user.name "Paul Webster"

git remote add e4 ../target.eclipse.platform.runtime.e4
git fetch e4
git merge e4/master

rm -f $MIGR_PASS/localize_e4_branches.sh

git branch -r | grep -v e4\/master >$MIGR_PASS/remote_e4_branches.txt
for f in $( cat $MIGR_PASS/remote_e4_branches.txt ); do 
BR=$( echo $f | sed 's/^.*\///g' )
echo git branch -t $BR $f >>$MIGR_PASS/localize_e4_branches.sh
done

/bin/bash $MIGR_PASS/localize_e4_branches.sh
git remote rm e4


cd $ROOT
cat - >gitignore <<EOF
bin/
*~
*.rej
*.bak
*.patch
javacore.*
heapdump.*
core.*
Snap.*
target/

EOF

cd target.$MODULE_NAME

# from http://stackoverflow.com/questions/3895453/how-do-i-make-a-git-commit-in-the-past
rm -f t1.txt
git checkout master

new_file=$(git hash-object -w ../gitignore)
git filter-branch \
--index-filter \
'git update-index --add --cacheinfo 100644 '"$new_file"' .gitignore' \
--tag-name-filter cat \
-- --all

git reset --hard

#create deploy
cd $ROOT
git clone --bare  target.$MODULE_NAME deploy.$MODULE_NAME
cd deploy.$MODULE_NAME
git remote rm origin
git config -l
git config --add core.sharedrepository 1

#final pack and prune step
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250


scp -r * pwebster@git.eclipse.org:/gitroot/platform/${MODULE_NAME}.git
#
# DONE, eclipse.platform.runtime
#


##################################################
# eclipse.platform.ui : merge in eclipse.platform.ui.e4

MODULE_NAME=eclipse.platform.ui
MIGR_PASS=$MIGR/$MODULE_NAME/pass3
MIGR_E4_PASS=$MIGR/${MODULE_NAME}.e4/pass4

cd $ROOT
rm -f t1 t2
sort -u $MIGR_PASS/tags.txt >t1
sort -u $MIGR_E4_PASS/tags.txt >t2
comm -12 t1 t2  >$MIGR_PASS/common_e4_tags.txt

# normally we would clean up the smaller repo, but it turns out these are
# just tags on one bundle, so we'll move them in the new repo:

cd target.$MODULE_NAME
for TAG in $( cat $MIGR_PASS/common_e4_tags.txt ); do
  git tag ${TAG}_ui ${TAG}
  git tag -d ${TAG}
done

cd $ROOT
rm -f t1 t2
sort -u $MIGR_PASS/branches.txt >t1
sort -u $MIGR_E4_PASS/branches.txt >t2
comm -12 t1 t2 >$MIGR_PASS/common_e4_branches.txt

# Only has branch master in common, that's not a problem


# Fix R4_HEAD ... otherwise the merge will remove all the other plugins :-)
# find the commits using git log
cd target.$MODULE_NAME
git log --boundary --pretty=oneline --decorate master..R4_HEAD >$MIGR_PASS/R4_HEAD_old.txt


#cvs2svn 5021f5d2d046d0d89f130b761bbeec401fa368f6 # delete commit
#bbokowski R3_6 e7b16db21a01ad9e587694d915cfa74a1787538c # parent commit
git rebase --onto e7b16db21a01ad9e587694d915cfa74a1787538c 5021f5d2d046d0d89f130b761bbeec401fa368f6

# add back any tags.  You have to manually look at the history, diff doesn't help
git log --boundary --pretty=oneline --decorate master..R4_HEAD >$MIGR_PASS/R4_HEAD_new.txt
# comparing them in gedit gives:
git tag -f v20110614-1530_ui  41e640fd2ad259b48d7a9b28e9355c2e7c20d285
git tag -f R4_1_ui  41e640fd2ad259b48d7a9b28e9355c2e7c20d285
git tag -f v20110613-2030_ui 6894fa950df61331dfb3db429bbbfcf6df6b8d39
git tag -f v20110506-1500_ui 9ac8056bf5a9354bc126ae02c9019af07e24a303
git tag -f v20110304-1630_ui 99cf89b77105884bef7a596f1a3223de15985f2a
git tag -f v20100924-1330_ui 6e2f2ea35a811e6823d0afa1fa382f037285725e
git tag -f v20100714-1430_ui 6f7a4a4d5ad0af130c5026ee78efe5ebf4089677
git tag -f v20100712-1900_ui 6f7a4a4d5ad0af130c5026ee78efe5ebf4089677
git tag -f R4_0_ui 6f7a4a4d5ad0af130c5026ee78efe5ebf4089677
git tag -f R0_10_ui 6f7a4a4d5ad0af130c5026ee78efe5ebf4089677
git tag -f v20100712-1400 36424bbc9647b8fa8a92645a8841be235dbd53e0



# if you are creating the main repo, you're OK


git checkout master
git merge R4_HEAD

# merge org.eclipse.ui/R4_HEAD into the 4.2 master
# did it using EGit and the http://wiki.eclipse.org/EGit/User_Guide#Resolving_a_merge_conflict
# Merge tool
    Conflicts:
        bundles/org.eclipse.ui/META-INF/MANIFEST.MF
        bundles/org.eclipse.ui/schema/viewActions.exsd


git remote add e4 ../target.${MODULE_NAME}.e4
git fetch e4
git merge e4/master

git status >$MIGR_PASS/merge_e4_ui_status.txt
# find a list of what was merged and conflicts
vi $MIGR_PASS/merge_e4_ui_status.txt

# just take the e4 workbench
git checkout --theirs bundles/org.eclipse.ui.workbench
git commit


# turn remote branches from e4.ui into local branches
rm -f $MIGR_PASS/localize_e4_branches.sh

git branch -r | grep -v e4\/master >$MIGR_PASS/remote_e4_branches.txt
for f in $( cat $MIGR_PASS/remote_e4_branches.txt ); do 
BR=$( echo $f | sed 's/^.*\///g' )
echo git branch -t $BR $f >>$MIGR_PASS/localize_e4_branches.sh
done

/bin/bash $MIGR_PASS/localize_e4_branches.sh
git remote rm e4

#
#  This merges eclipse.platform.ui.e4 into eclipse.platform.ui
#


# Now the final prep, eh?
git branch -d R4_HEAD
git tag pre_R4_HEAD_merge 3b015a2825106dc0335edf3f3698f0f90c6a5cb9
git branch R3_development pre_R4_HEAD_merge
git branch R4_development master
git tag R4_1_mainenance_tag R4_1_maintenance
git branch -D  R4_1_maintenance



cd $ROOT
cat - >gitignore <<EOF
bin/
*~
*.rej
*.bak
*.patch
javacore.*
heapdump.*
core.*
Snap.*
target/

EOF

cd target.$MODULE_NAME

# from http://stackoverflow.com/questions/3895453/how-do-i-make-a-git-commit-in-the-past
rm -f t1.txt
git checkout master

new_file=$(git hash-object -w ../gitignore)
git filter-branch \
--index-filter \
'git update-index --add --cacheinfo 100644 '"$new_file"' .gitignore' \
--tag-name-filter cat \
-- --all

git reset --hard

git checkout master
git reset --hard R3_development


#create deploy
cd $ROOT
git clone --bare  target.$MODULE_NAME deploy.$MODULE_NAME
cd deploy.$MODULE_NAME
git remote rm origin
git config -l
git config --add core.sharedrepository 1

git reflog expire --all
rm -rf refs/original

#final pack and prune step
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

rm -f gitk.cache

# cut, ship, and print
scp -r * pwebster@git.eclipse.org:/gitroot/platform/${MODULE_NAME}.git




##################################################
# next, org.eclipse.releng.eclipsebuilder since it should be isolated
ROOT=/shared/eclipse/e4/git/cvs
MIGR=$ROOT/org.eclipse.migration
MODULE_NAME=org.eclipse.releng.eclipsebuilder

cd $ROOT
CVS_LOC=$ROOT/cvsroot.$MODULE_NAME
mkdir -p $CVS_LOC/$MODULE_NAME
cd $CVS_LOC
cvs -d :local:$(pwd) init

MIGR_PASS=$MIGR/$MODULE_NAME/pass2

# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cd $ROOT
vi $MIGR_PASS/cvs2git.options
rm -rf cvs2svn-tmp symbol-info.txt cvs2git_log.txt
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt cvs2git_log.txt $MIGR_PASS



# create a git repo to be used for verification
git init verify.$MODULE_NAME
cd verify.$MODULE_NAME
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

git tag -d v/builds/N20050204-1608

# verify the repo - this can take long, so for your first test skip this step.
cd $ROOT
rm -rf compare-tmp
mkdir compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $CVS_LOC/$MODULE_NAME \
$ROOT/verify.$MODULE_NAME \
--tmp=$ROOT/compare-tmp --diff >$MIGR_PASS/compare-log.txt 2>&1 &

# create a git repo to continue the conversion
cd $ROOT
git init target.$MODULE_NAME
cd target.$MODULE_NAME

cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

git tag -d v/builds/N20050204-1608

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.


git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt


# last step before you deploy:

cd $ROOT
cd target.$MODULE_NAME


git reset --hard

#create deploy
cd $ROOT
git clone --bare  target.$MODULE_NAME deploy.$MODULE_NAME
cd deploy.$MODULE_NAME
git remote rm origin
git config -l
git config --add core.sharedrepository 1

#final pack and prune step
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

cd $ROOT
scp -r deploy.$MODULE_NAME pwebster@git.eclipse.org:/gitroot/e4/${MODULE_NAME}.git
#
# DONE, org.eclipse.releng.eclipsebuilder
#


##################################################
# next, org.eclipse.e4.languages since it should be isolated
MODULE_NAME=org.eclipse.e4.languages

cd $ROOT
CVS_LOC=$ROOT/cvsroot.$MODULE_NAME
mkdir -p $CVS_LOC/e4
cd $CVS_LOC
cvs -d :local:$(pwd) init

MIGR_PASS=$MIGR/org.eclipse.e4.languages/pass1
cp -r /cvsroot/eclipse/e4/$MODULE_NAME $CVS_LOC/e4

# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cd $ROOT
vi $MIGR_PASS/cvs2git.options
rm -rf cvs2svn-tmp symbol-info.txt cvs2git_log.txt
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt cvs2git_log.txt $MIGR_PASS



# create a git repo to be used for verification
git init verify.$MODULE_NAME
cd verify.$MODULE_NAME
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

# verify the repo - this can take long, so for your first test skip this step.
cd $ROOT
rm -rf compare-tmp
mkdir compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $CVS_LOC/e4/$MODULE_NAME \
$ROOT/verify.$MODULE_NAME \
--tmp=$ROOT/compare-tmp --diff >$MIGR_PASS/compare-log.txt 2>&1 &

# create a git repo to continue the conversion
cd $ROOT
git init target.$MODULE_NAME
cd target.$MODULE_NAME

cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250



# Fixing the Delete only tags:
cd $ROOT/target.$MODULE_NAME
/bin/bash $MIGR/scripts/fix_tags.sh >$MIGR_PASS/tag_report.sh
grep ^git $MIGR_PASS/tag_report.sh >$MIGR_PASS/mv_tags.sh
# examine them
/bin/bash $MIGR_PASS/mv_tags.sh

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.

grep ^Cherry $MIGR_PASS/tag_report.sh >$MIGR_PASS/tag_cherrypicks.txt
grep "R[34]_" $MIGR_PASS/tag_cherrypicks.txt
# No R?_ Cherry pick tags

git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt

# last step before you deploy:

cd $ROOT
cat - >gitignore <<EOF
bin/
*~
*.rej
*.bak
*.patch
javacore.*
heapdump.*
core.*
Snap.*
target/

EOF

cd target.$MODULE_NAME

# from http://stackoverflow.com/questions/3895453/how-do-i-make-a-git-commit-in-the-past

new_file=$(git hash-object -w ../gitignore)
git filter-branch \
--index-filter \
'git update-index --add --cacheinfo 100644 '"$new_file"' .gitignore' \
--tag-name-filter cat \
-- --all

git reset --hard

#create deploy
cd $ROOT
git clone --bare  target.$MODULE_NAME deploy.$MODULE_NAME
cd deploy.$MODULE_NAME
git remote rm origin
git config -l
# git config --add core.sharedrepository 1
cp /gitroot/platform/eclipse.platform.ui.git/hooks/pre-receive hooks

#final pack and prune step
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

cd $ROOT
scp -r deploy.$MODULE_NAME pwebster@build.eclipse.org:/gitroot/e4/${MODULE_NAME}.git
#
# DONE, org.eclipse.e4.languages
#

##################################################
# next, eclipse.platform.incubator since it should be isolated
MODULE_NAME=eclipse.platform.incubator

cd $ROOT
CVS_LOC=$ROOT/cvsroot.$MODULE_NAME
mkdir -p $CVS_LOC/e4
cd $CVS_LOC
cvs -d :local:$(pwd) init

MIGR_PASS=$MIGR/eclipse.platform.incubator/pass1
cp -r /cvsroot/eclipse/e4/$MODULE_NAME $CVS_LOC/e4

# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cd $ROOT
vi $MIGR_PASS/cvs2git.options
rm -rf cvs2svn-tmp symbol-info.txt cvs2git_log.txt
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt cvs2git_log.txt $MIGR_PASS



# create a git repo to be used for verification
git init verify.$MODULE_NAME
cd verify.$MODULE_NAME
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

# verify the repo - this can take long, so for your first test skip this step.
cd $ROOT
rm -rf compare-tmp
mkdir compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $CVS_LOC/e4/$MODULE_NAME \
$ROOT/verify.$MODULE_NAME \
--tmp=$ROOT/compare-tmp --diff >$MIGR_PASS/compare-log.txt 2>&1 &

# create a git repo to continue the conversion
cd $ROOT
git init target.$MODULE_NAME
cd target.$MODULE_NAME

cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250



# Fixing the Delete only tags:
cd $ROOT/target.$MODULE_NAME
/bin/bash $MIGR/scripts/fix_tags.sh >$MIGR_PASS/tag_report.sh
grep ^git $MIGR_PASS/tag_report.sh >$MIGR_PASS/mv_tags.sh
# examine them
/bin/bash $MIGR_PASS/mv_tags.sh

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.

grep ^Cherry $MIGR_PASS/tag_report.sh >$MIGR_PASS/tag_cherrypicks.txt
grep "R[34]_" $MIGR_PASS/tag_cherrypicks.txt
# No R?_ Cherry pick tags

git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt

# last step before you deploy:

cd $ROOT
cat - >gitignore <<EOF
/*/*/bin/
*~
*.rej
*.bak
*.patch
javacore.*
heapdump.*
core.*
Snap.*
/*/*/target/

EOF

cd target.$MODULE_NAME

# from http://stackoverflow.com/questions/3895453/how-do-i-make-a-git-commit-in-the-past

new_file=$(git hash-object -w ../gitignore)
git filter-branch \
--index-filter \
'git update-index --add --cacheinfo 100644 '"$new_file"' .gitignore' \
--tag-name-filter cat \
-- --all

git reset --hard

#create deploy
cd $ROOT
git clone --bare  target.$MODULE_NAME deploy.$MODULE_NAME
cd deploy.$MODULE_NAME
git remote rm origin
git config -l
# git config --add core.sharedrepository 1
cp /gitroot/platform/eclipse.platform.ui.git/hooks/pre-receive hooks

#final pack and prune step
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

cd $ROOT
scp -r deploy.$MODULE_NAME pwebster@build.eclipse.org:/gitroot/e4/${MODULE_NAME}.git
#
# DONE, eclipse.platform.incubator
#

##################################################
# next, eclipse.platform.incubator since it should be isolated
MODULE_NAME=org.eclipse.e4.resources

cd $ROOT
CVS_LOC=$ROOT/cvsroot.$MODULE_NAME
mkdir -p $CVS_LOC/e4
cd $CVS_LOC
cvs -d :local:$(pwd) init

MIGR_PASS=$MIGR/org.eclipse.e4.resources/pass1
cp -r /cvsroot/eclipse/e4/$MODULE_NAME $CVS_LOC/e4

# clean the CVS repo of links
find $CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cd $ROOT
vi $MIGR_PASS/cvs2git.options
rm -rf cvs2svn-tmp symbol-info.txt cvs2git_log.txt
cvs2svn-trunk/cvs2git --options=$MIGR_PASS/cvs2git.options >cvs2git_log.txt 2>&1
cp symbol-info.txt cvs2git_log.txt $MIGR_PASS



# create a git repo to be used for verification
git init verify.$MODULE_NAME
cd verify.$MODULE_NAME
cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import

python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

# verify the repo - this can take long, so for your first test skip this step.
cd $ROOT
rm -rf compare-tmp
mkdir compare-tmp
python $ROOT/cvs2svn-trunk/contrib/verify-cvs2svn.py \
--git $CVS_LOC/e4/$MODULE_NAME \
$ROOT/verify.$MODULE_NAME \
--tmp=$ROOT/compare-tmp --diff >$MIGR_PASS/compare-log.txt 2>&1 &

# create a git repo to continue the conversion
cd $ROOT
git init target.$MODULE_NAME
cd target.$MODULE_NAME

cat ../cvs2svn-tmp/git-blob.dat ../cvs2svn-tmp/git-dump.dat | git fast-import
python ../cvs2svn-trunk/contrib/git-move-refs.py
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250



# Fixing the Delete only tags:
cd $ROOT/target.$MODULE_NAME
/bin/bash $MIGR/scripts/fix_tags.sh >$MIGR_PASS/tag_report.sh
grep ^git $MIGR_PASS/tag_report.sh >$MIGR_PASS/mv_tags.sh
# examine them
/bin/bash $MIGR_PASS/mv_tags.sh

# now examine the repo, hopefully the tags and branches that we care about are in decent shape.
# In my case, I see Cherrypick markers for a couple of tags.  When the conversion is complete and most
# people are happy, a developer will have to examine these commits to see if they
# can be simplified.

grep ^Cherry $MIGR_PASS/tag_report.sh >$MIGR_PASS/tag_cherrypicks.txt
grep "R[34]_" $MIGR_PASS/tag_cherrypicks.txt
# No R?_ Cherry pick tags

git branch >$MIGR_PASS/branches.txt
git tag >$MIGR_PASS/tags.txt

# last step before you deploy:

cd $ROOT
cat - >gitignore <<EOF
/*/*/bin/
*~
*.rej
*.bak
*.patch
javacore.*
heapdump.*
core.*
Snap.*
/*/*/target/

EOF

cd target.$MODULE_NAME

# from http://stackoverflow.com/questions/3895453/how-do-i-make-a-git-commit-in-the-past

new_file=$(git hash-object -w ../gitignore)
git filter-branch \
--index-filter \
'git update-index --add --cacheinfo 100644 '"$new_file"' .gitignore' \
--tag-name-filter cat \
-- --all

git reset --hard

#create deploy
cd $ROOT
git clone --bare  target.$MODULE_NAME deploy.$MODULE_NAME
cd deploy.$MODULE_NAME
git remote rm origin
git config -l
# git config --add core.sharedrepository 1
cp /gitroot/platform/eclipse.platform.ui.git/hooks/pre-receive hooks

#final pack and prune step
git prune
git repack -a -d --depth=250 --window=250
git gc --aggressive
git repack -a -d --depth=250 --window=250

cd $ROOT
scp -r deploy.$MODULE_NAME pwebster@build.eclipse.org:/gitroot/e4/${MODULE_NAME}.git
#
# DONE, org.eclipse.e4.resources
#