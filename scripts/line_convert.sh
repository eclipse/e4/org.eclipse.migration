#!/bin/bash

  rm -f new.map
  while read LINE; do 
    if (echo $LINE | grep :pserver >/dev/null); then 
      HDR=$( echo $LINE | cut -f1 -d= ) 
      ID=$( echo $HDR | cut -f2 -d\@ )
      TAG=$( echo $LINE | cut -f1 -d, | cut -f2 -d= )
      echo "${HDR}=GIT,tag=${TAG},repo=git://git.eclipse.org/gitroot/e4/eclipse.platform.ui.git,path=bundles/$ID" >>new.map
    else
      echo $LINE >>new.map
    fi
  done <ui.map

