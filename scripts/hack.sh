#!/bin/bash
#

#TAG=R3_6

for TAG in $( git tag ); do

git log -1 --format="format:%H:%an:%b" $TAG >t1.txt
if grep cvs2svn t1.txt >/dev/null; then
CMT=$( head -1 t1.txt | cut -f1 -d: )
if ! grep ^Cherrypick t1.txt >/dev/null; then
	#echo Found cvs2svn $CMT
	if [ -z "$(git log -1  --merges --format="format:%h" $TAG)" ]; then
		PT=$( git log -1 --format="format:%an" "${CMT}^1" )
		if [ "$PT" != cvs2svn ]; then
			echo git tag -f "$TAG" "${CMT}^1"
		fi
	fi
else
	echo "Cherrypick tag: $TAG commit: $CMT"
fi
fi

done

