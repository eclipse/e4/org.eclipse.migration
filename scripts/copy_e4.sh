#!/bin/bash
#

if (( $# != 2 )); then
	echo "USAGE: $0 <module_file> <target_cvs_repo_dir>"
	exit 1
fi

if [ -r "$1" ]; then
        . "$1"
else
	echo "USAGE: $0 <module_file> <target_cvs_repo_dir>"
        exit 1
fi

BASE_DIR="$2"

for M in $MODULES; do
	TARGET_DIR=$BASE_DIR/$M
	PARENT=$( dirname $TARGET_DIR )
	if [ ! -e "$PARENT" ]; then
		mkdir -p $PARENT
	fi
	echo cp -r /cvsroot/eclipse/$M $PARENT
	cp -r /cvsroot/eclipse/$M $PARENT
done

