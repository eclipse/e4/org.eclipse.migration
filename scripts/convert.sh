#!/bin/bash

MAP=new.map

rm -f $MAP

sed 's/^\([^@]*\)@\([^=]*\)=\([^,]*\),.*$/\1@\2=GIT,tag=\3,repo=git:\/\/git.eclipse.org\/gitroot\/e4\/eclipse.platform.ui.git,path=bundles\/\2/g' ui.map >$MAP

