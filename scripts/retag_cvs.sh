#!/bin/bash

if (( $# != 4 )); then
	echo "USAGE: $0 <module_file> TAG \"MM/DD/YYYY HH:mm\" BRANCH"
	exit 1
fi


if [ -r "$1" ]; then
	. "$1"
else
	echo "USAGE: $0 <module_file> TAG \"MM/DD/YYYY HH:mm\" BRANCH"
	exit 1
fi

# what base tag at what date, and what branch
TAG="$2"
TDATE="$3"
BRANCH="$4"


for M in $MODULES; do

	if (cvs rlog -rHEAD $M/.project | grep "	${TAG}:" >/dev/null); then
		if (cvs rlog  -r${TAG} $M/.project | grep "	${BRANCH}:" >/dev/null); then
			echo Correct $BRANCH for $M
		else
			echo cvs rtag -b -r$TAG $BRANCH $M
		fi
	else
		if ( cvs rlog -d "$TDATE" $M/.project | grep "selected revisions: 0" >/dev/null ); then
			echo "Module $M empty on $TDATE"
		else
			echo cvs rtag -D\"$TDATE\" $TAG $M
			echo cvs rtag -b -r$TAG $BRANCH $M
		fi
	fi
done

