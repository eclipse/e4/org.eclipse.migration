#!/bin/bash

git filter-branch --parent-filter \
'IN=$( cat ); \
if test $( echo $IN | wc -w ) = 2; then \
	P1=$( echo $IN | cut -f2 -d" " ); \
	if test "$( git log -1 --format="%an"  $P1 )" = cvs2svn; then \
		if (git log -1 --format="%s" $P1 | grep "cvs2svn to create branch" >/dev/null); then \
		if ! (git log -1 --format="%b" $P1 | grep ^Cherrypick >/dev/null); then \
			P_COMMIT=$( git log -1 --format="%P" $P1 ); \
			if test "$( git log -1 --format="%an" $P_COMMIT )" != cvs2svn; then \
				echo "-p $P_COMMIT" ; \
			else echo "$IN" ; fi ; \
		else echo "$IN" ; fi ; \
		else echo "$IN" ; fi ; \
	else echo "$IN" ; fi ; \
else echo "$IN" ; fi ' \
--tag-name-filter 'cat' -- --all

#git filter-branch --parent-filter \
#'test $( git log -1 --format="%an"  $( cut -f2 -d" ") ) = cvs2svn && \
#(git log -1 --format="%b"  $( cut -f2 -d" ") | grep "cvs2svn to create branch" >/dev/null ) && \
#test $(git log -1 --format="%an" $( git log -1 --format="%P" $( cut -f2 -d" ")  ) ) != cvs2svn && \
#echo "-p $( git log -1 --format="%P" $( git log -1 --format="%P" $( cut -f2 -d" ")  ) )" || \
#cat ' \
#--tag-name-filter 'cat'


#git filter-branch --parent-filter \
#' test $( git log -1 --format="%an" $GIT_COMMIT ) = cvs2svn && \
#(git log -1 --format="%b" $GIT_COMMIT | grep "cvs2svn to create branch" >/dev/null ) && \
#P_COMMIT=$( git log -1 --format="%P" $GIT_COMMIT ) && \
#test $(git log -1 --format="%an" $P_COMMIT ) != cvs2svn && \
#echo "-p $( git log -1 --format="%P" $P_COMMIT )" || \
#cat ' \
#--tag-name-filter 'cat'

