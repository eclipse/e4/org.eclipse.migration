#!/bin/bash
# This needs more work to deal with the "new" mapfile format:
# plugin@id=CVS,tag=...
#


ROOT=/opt/pwebster/git/R42
WS=/opt/pwebster/git/R42

REPOS='
git://git.eclipse.org/gitroot/e4/org.eclipse.e4.xwt.git
'

cd $ROOT

update_map () {
	REPO=$1
	REPO_DIR=$( basename $REPO .git )
	M=$2
	ID=$3
	MAP=$4
	if [ ! -z "$5" ]; then
		echo Extra map $5
	fi
	
	REPO_PATH=$( echo $REPO | sed 's/\//\\\//g' )
	M_PATH=$( echo $M | sed 's/^[^/]*\///g' | sed 's/\//\\\//g' )
	echo sed "'s/@${ID}=\([^,]*\),.*$/@${ID}=GIT,tag=\1,repo=${REPO_PATH},path=${M_PATH}/g' $MAP >t1.txt ; mv t1.txt $MAP "
}


# find the map files
for REPO in $REPOS; do
	REPO_DIR=$( basename $REPO .git )
	MODULES=$( ls -d $REPO_DIR/*/* )
	for M in $MODULES; do
		ID=$( basename $M )
		#MAP=$( find $WS/org.eclipse.releng/maps -name "*.map" -exec grep -l "@${ID}=" {} \; )
		#if [ ! -z "$MAP" ]; then
			#update_map $REPO $M $ID $MAP
		#fi
		MAP=$( find $WS/org.eclipse.e4.releng -name "*.map" -exec grep -l "@${ID}=" {} \; )
		if [ ! -z "$MAP" ]; then
			update_map $REPO $M $ID $MAP
		fi
	done
done

