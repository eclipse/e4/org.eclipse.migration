#!/bin/bash

if (( $# != 3 )); then
	echo "USAGE: $0 <module_file> TAG NEXT_TAG"
	exit 1
fi


if [ -r "$1" ]; then
	. "$1"
else
	echo "USAGE: $0 <module_file> TAG NEXT_TAG"
	exit 1
fi


# what base tag at what date, and what branch
TAG1="$2"
TAG2="$3"

for M in $MODULES; do
	if (cvs rlog -rHEAD $M/.project | grep "	${TAG1}:" >/dev/null); then
		if ! (cvs rlog -rHEAD $M/.project | grep "	${TAG2}:" >/dev/null); then
			echo cvs rtag -R -r${TAG1} ${TAG2} $M
		else
			echo Already tagged ${TAG2} $M
		fi
	else
		echo "Module $M empty or not tagged, skipping"
	fi
done

