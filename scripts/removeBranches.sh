#!/bin/bash
#

if [ $# -lt 1 ]; then
echo "USAGE: $0 <branch_file>"
exit 1
fi

BRANCH_LIST="$1" ; shift

for ORIG_BRANCH in $( cat $BRANCH_LIST ); do
	BRANCH=$( echo $ORIG_BRANCH | sed 's![ ]*origin/!!g' )
	TAG=zArchived/$BRANCH
	echo git tag $TAG $ORIG_BRANCH
	echo git push origin $TAG
	echo git push origin :$BRANCH
done

